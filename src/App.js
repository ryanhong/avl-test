import React from 'react';
import logo from './logo.svg';
import './App.css';
import Email from './email.js';
import Problem from './Problem.js';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { Router, Route, Switch, Redirect } from "react-router-dom";
import ReactDOM from "react-dom";
import { createBrowserHistory, createHashHistory } from "history";
const browserHistory = createBrowserHistory();

const themeX = createMuiTheme({
  palette: {
    type: "dark",
    grey: {
      800: "#000000", // overrides failed
      900: "#121212" // overrides success
    }
  }
});

function App() {
  return (
    <ThemeProvider theme={themeX}>
      <Router history={browserHistory}>
        <Switch>
          <Route path="/problem" component={Problem} />
          <Route path="/" component={Email} />
          <Redirect to='/' />
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
