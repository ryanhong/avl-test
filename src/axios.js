'use strict';

/**
 * Axios Handler
 *
 * @category    Component
 * @author      Ryan Hong
 * @copyright   Copyright (c) Ovomedia Creative Inc.
 */

import ReactDOM from 'react-dom';
import React, { Component }  from 'react';

const axios = require('axios');

const instance = axios.create({
	responseType: 'json',
	baseURL: process.env.REACT_APP_API_URL,
	headers: {
	  'Access-Control-Allow-Origin': '*',
	},
});

export default axios;
