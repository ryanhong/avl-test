import React, { Component, useEffect } from 'react'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import axios from './axios.js';

const themeX = createMuiTheme({
  palette: {
    type: "dark",
    grey: {
      800: "#000000", // overrides failed
      900: "#121212" // overrides success
    }
  }
});

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    height: '30rem'
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function Problem() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  const [response, setResponse] = React.useState({});

  useEffect(() => {
    axios.get('https://us-central1-test-7e215.cloudfunctions.net/getData/工作表1')
    .then((response) => {
      setResponse(response.data);
    })
    .catch(function (error) {
    })
    .then(() => {
    });
  }, []);
  return (
      <table>
        <th>question_text</th>
        <th>question_title</th>
        <th>hashtags</th>
        {Object.values(response).map((data) => (
          <tr>
            <td>{data.question_text}</td>
            <td>{data.question_title}</td>
            <td>{data.hashtags}</td>
          </tr>
        ))}
      </table>
  );
}
