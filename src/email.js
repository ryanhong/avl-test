import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import axios from './axios.js';

const themeX = createMuiTheme({
  palette: {
    type: "dark",
    grey: {
      800: "#000000", // overrides failed
      900: "#121212" // overrides success
    }
  }
});

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    height: '30rem'
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function Email() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  const [subject, setSubject] = React.useState("Hello");
  const [content, setContent] = React.useState("blabla");

  function sendmail() {
    axios.post('https://us-central1-test-7e215.cloudfunctions.net/emailMessage', {
      subject: subject,
      content: content,
    })
    .then((response) => {
      alert('送出成功！');
    })
    .catch(function (error) {
    })
    .then(() => {
    });
  }

  const handleChangeSubject = (e) => {
    let value = e.target.value;
    setSubject(e.target.value)
  }

  const handleChangeContent = (e) => {
    console.log(e.target.value)
    let value = e.target.value;
    setContent(e.target.value)
  }

  return (
    <ThemeProvider theme={themeX}>
      <Card className={classes.root}>
        <CardContent style={{textAlign: 'left'}}>
          <Typography variant="h4" component="h4">
            Hello Jen
          </Typography>
          <Typography style={{paddingBottom: '16px'}} variant="h5" component="h5">
            Feel free to ask us anything
          </Typography>
          <TextField style={{paddingBottom: '16px'}} required fullWidth onChange={handleChangeSubject} name="subject" id="standard-required" label="Required" value={subject} />
          <TextField
            style={{paddingBottom: '16px'}} 
            fullWidth
            name="content"   
            id="standard-multiline-static"
            label="Multiline"
            multiline
            rows={4}
            value={content}
            onChange={handleChangeContent}
          />
          <Button variant="contained" style={{textAlign: 'center'}} color="primary" onClick={sendmail}>
            Send
          </Button>
        </CardContent>
      </Card>
    </ThemeProvider>
  );
}
